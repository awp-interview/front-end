import './App.css';
import { People } from './people/People';
import { Products } from './products/Products';

// TODO: change the page title to "Demo"

const App = () => {
  return (
    <div className="App">
      <People />
      <Products />
    </div>
  );
}

export default App;
