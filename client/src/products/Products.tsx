import { useState } from "react";
import { Product } from "./Product";
import "./products.css";

export const Products = () => {
  const [total, setTotal] = useState(0);

  // TODO get "random products" from API product and display name of each product
  const handleClick = () => {
    // get products and update total
    setTotal(total + 1)
  }

  return (
    <section className="products">
      <h2 className="products-title">{`Products (${total})`}</h2>
      <button className="products-button" onClick={handleClick}>Get Products</button>
      <br />
      {/* // TODO: have button that gets new products */}
      <ul className="products-list">
        {/* // TODO iterate source data and display product title, cost, and description. */}
        <Product product={{ cost: "$15.99", description: "Sample product description text." }} initialTotal={total} />
        <Product product={{ cost: "$15.99", description: "Sample product description text." }} initialTotal={total} />
        <Product product={{ cost: "$15.99", description: "Sample product description text." }} initialTotal={total} />
        <Product product={{ cost: "$15.99", description: "Sample product description text." }} initialTotal={total} />
      </ul>
    </section>
  )
}
