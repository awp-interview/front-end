import React from 'react'

export const Product = ({ product, initialTotal }: any) => {
  const [title] = React.useState(product.name)
  const [productTotal] = React.useState(initialTotal)

  return (
    <li className="product">
      <span>{title}</span>
      <br />
      <span className="product-details">{`ID: ${product?.id} of ${productTotal}`}</span>
      <br />
      <span className="product-details">{`Cost: ${product?.cost}`}</span>
      <br />
      <span className="product-details">{`Description: ${product?.description}`}</span>
    </li>
  )
}
