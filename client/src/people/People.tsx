import { useEffect, useState } from 'react'
import "./people.css";
import IPerson from './person.interface';

export const People = () => {
  const [people, setPeople] = useState<IPerson[]>([])

  useEffect(() => {
    const url = '000https://jsonplaceholder.typicode.com/users'
    const controller = new AbortController();
    const signal = controller.signal;

    fetch(url, { signal })
      .then(resp => resp.json())
      .then((res) => setPeople(res))
      .catch((err) => console.error("error during fetch: ", err.message))

    return () => {
    }
  }, [])

  return (
    <section className="people" id="people">
      <h2 className="people-title">People</h2>
      {/* // TODO iterate and pass to Person component. Have Person component display name and email */}
      {people && JSON.stringify(people)}
      <ul className="people-list">
      </ul>
    </section>
  )
}
