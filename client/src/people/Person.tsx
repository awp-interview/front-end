import { useState } from "react"
import user from "../assets/user.svg";

export const Person = ({ person }: any) => {
  const [name] = useState(person.name)

  return (
    <li className="person" id="person">
      <img width={50} height={50} alt="sample" src={user} />
      <div>
        <span>{name}</span>
        <br />
        <small className="person-details">{` email: ${person.website}`}</small>
      </div>
    </li>
  )
}
