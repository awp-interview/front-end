import { Injectable, Logger } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import * as productsList from './static/products.json';

@Injectable()
export class ProductsService {
  private readonly logger = new Logger(ProductsService.name);
  private products: Product[] = productsList;
  private idTotal: number = productsList.length;

  create(createProductDto: CreateProductDto) {
    this.idTotal = ++this.idTotal;
    this.products.push({ ...createProductDto, id: this.idTotal });
    return this.products[this.products.length - 1];
  }

  findAll() {
    const sortedProducts = [...this.products].sort(() => 0.5 - Math.random());
    this.logger.log(`Returned product length: ${sortedProducts.length}`);
    return sortedProducts;
  }

  findRandom() {
    return this.products[Math.floor(Math.random() * this.products.length)];
  }

  findOne(id: number) {
    // TODO find and return specific product
    return `This action returns a #${id} product`;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    return `This action updates a #${id} product`;
  }

  remove(id: number) {
    const updatedProducts = this.products.filter((prod) => id !== prod.id);
    this.products = updatedProducts;
    return updatedProducts;
  }

  deleteAll() {
    return (this.products = []);
  }
}
